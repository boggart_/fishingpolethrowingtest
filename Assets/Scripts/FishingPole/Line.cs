﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

///
/// This script draws the line with the given Transforms positions
/// It requires a line render in order to avoid Null Reference while getting component.
/// And it needs to be executed in Edit Mode to we can see the line drawn while moving transforms on scene.
///
[RequireComponent(typeof(LineRenderer))]
[ExecuteInEditMode]
public class Line : MonoBehaviour
{
    public List<Transform> fixedLinePositions;
    
    public float startWidth;
    public float endWith;
    private LineRenderer line;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();

        line.positionCount = fixedLinePositions.Count;
        line.startWidth = startWidth;
        line.endWidth = endWith;

        line.SetPositions(fixedLinePositions.Select(p=>p.position).ToArray());
    }

    private void LateUpdate()
    {
        line.SetPositions(fixedLinePositions.Select(p => p.position).ToArray());
    }
}
