﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///
/// This script manages the animator of the fishing pole. 
/// It requires an Animator Component to not have null reference while getting Animator component in Awake.
/// The method Throw is executed when the 'Throw In' button is pressed

[RequireComponent(typeof(Animator))]
public class FishingPole : MonoBehaviour
{
    public ParticleSystem wavesPS;
    public ParticleSystem splashPS;
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.T))
        {
            Throw();
        }
#endif
    }

    public void Throw()
    {
        anim.SetTrigger("throw");
    }

    public void Splash()
    {
        wavesPS.gameObject.SetActive(true);
        wavesPS.Play();
        splashPS.Play();
    }

}
