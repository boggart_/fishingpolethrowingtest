﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages elements on scene, for now only controlls one fish its spawning points and appearing time.
/// </summary>
public class ScenaryManager : MonoBehaviour
{
    [SerializeField]
    float minJumpingDelay;
    [SerializeField]
    float maxJumpingDelay;

    [SerializeField]
    Fish queenAngel;

    [SerializeField]
    Transform[] spawningPoints;

    private float jumpingTimer;

    private void Start()
    {
        jumpingTimer = Random.Range(minJumpingDelay,maxJumpingDelay);
    }
    private void Update()
    {
        jumpingTimer -= Time.deltaTime;
        if(jumpingTimer <= 0)
        {
            int randomSpawningPoint = Random.Range(0, spawningPoints.Length);
            queenAngel.Jump(spawningPoints[randomSpawningPoint].position);
            jumpingTimer = Random.Range(minJumpingDelay, maxJumpingDelay);
        }
    }

}
