﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Detects collisions with water to display particles
/// </summary>
public class WaterBehaviour : MonoBehaviour
{
    [SerializeField]
    ParticleSystem splashParticles;
    [SerializeField]
    Pool wavesPool;

    [SerializeField]
    Vector3 wavesPositionOffset;
    [SerializeField]
    Vector3 splashPositionOffset;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Fish")
        {
            Vector3 collisionPoint = collision.GetContact(0).point;

            GameObject newSplash = wavesPool.GetObject();
            newSplash.transform.position = collisionPoint + wavesPositionOffset;
            newSplash.SetActive(true);

            splashParticles.transform.position = collisionPoint + splashPositionOffset;
            splashParticles.Play();
        }
    }
}
