﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Fish : MonoBehaviour
{
    [SerializeField]
    Animator anim;
  
    /// <summary>
    /// Sets the starting position of the fish and triggers the jump animation.
    /// </summary>
    public void Jump(Vector3 _worldPos)
    {

        // Deciding fish jumping direction 
        bool jumpToRight = Random.Range(0, 10) > 5;

        if(jumpToRight)
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 90);
        }
        else
        {
            transform.rotation = Quaternion.Euler(Vector3.up * -90);
        }

        transform.position = _worldPos;
        anim.SetTrigger("jump");
    }
}
